package edu.bigdata.training.testservice.controller;

import edu.bigdata.training.testservice.controller.model.Person;
import edu.bigdata.training.testservice.model.PersonEntity;
import edu.bigdata.training.testservice.service.TestBusinessLogicService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/person")
public class TestServiceController {

    private TestBusinessLogicService testBusinessLogicService;

    public TestServiceController(TestBusinessLogicService testBusinessLogicService) {
        this.testBusinessLogicService = testBusinessLogicService;
    }

    //person create
    @PostMapping(path = {"/"}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonEntity> createPerson(@RequestBody Person person) {
        PersonEntity personEntity = testBusinessLogicService.processCreate(person);
        return new ResponseEntity<>(personEntity, HttpStatus.OK);
    }

    //get id
    @GetMapping(path = {"/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonEntity> getPerson(@PathVariable String id) {
        PersonEntity personEntity = testBusinessLogicService.processGet(id);
        return new ResponseEntity<>(personEntity, HttpStatus.OK);
    }

    //delete id
    @DeleteMapping(path = {"/{id}"})
    public ResponseEntity<PersonEntity> delPerson(@PathVariable String id) {
        testBusinessLogicService.processDel(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    //put id
    @PutMapping(path = {"/{id}"})
    public ResponseEntity<PersonEntity> putPerson(@RequestBody Person person, @PathVariable String id) {
        PersonEntity personEntity = testBusinessLogicService.processPut(person, id);
        return new ResponseEntity<>(personEntity, HttpStatus.OK);
    }

    //get all
    @GetMapping(path = {"/"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PersonEntity>> getAll() {
        List<PersonEntity> personEntities = testBusinessLogicService.processGetAll();
        return new ResponseEntity<>(personEntities, HttpStatus.OK);
    }
}
